const process = require('process');

const dotenv = require('dotenv');
dotenv.config();
if(process.env.DOTENV_SECONDARY_PATH) {
    dotenv.config({path: process.env.DOTENV_SECONDARY_PATH});
}

const fs = require('fs');
const express = require('express');
const sharp = require('sharp')

const http = require('http');
const https = require('https');

const MAX_WIDTH = (process.env?.MAX_WIDTH && parseInt(process.env.MAX_WIDTH)) || 1920;
const MAX_HEIGHT = (process.env?.MAX_HEIGHT && parseInt(process.env.MAX_HEIGHT)) || 1080;

const ALLOWED_HOSTS = new Set(process.env?.ALLOWED_HOSTS?.split?.(',')?.filter?.(x => x) || []);

const FORCE_HOSTS_BASE_URL = process.env?.FORCE_HOSTS_BASE_URL?.split?.(',')?.map?.(x => x?.split?.('>'))?.filter?.(x => x && x?.length == 2) || [];

function resize(path, format, width, height, quality) {
    return new Promise((resolve, reject) => {
        const client = /^https/.test(path) ? https : http;

        const request = client.get(path, (res) => {
            res.setEncoding('binary');
             if (res.statusCode !== 200) {
                console.error(`Did not get an OK from the server. Code: ${res.statusCode}`);
                res.resume();
                reject(`Invalid status code from server: ${res.statusCode}`);
              } else {

                  const data = [];

                  res.on('data', (chunk) => {
                    data.push(Buffer.from(chunk, 'binary'));
                  });

                  res.on('close', () => {
                    let transform;
                    try {
                        transform = sharp(Buffer.concat(data)).timeout({seconds: 10});
                    } catch(error) {
                        console.error(error);
                        reject(`Encountered an error trying to instantiate resizer: ${error.message}`);
                        return;
                    }

                    // const metadata = await transform.metadata()

                    // let targetWidth;
                    // let targetHeight;
                    // let scale;

                    // if(width) {
                    //     if(width > 1920) {
                    //         width = 1920;
                    //     }
                    //     if(!height) {
                    //         targetWidth = width;
                    //         scale = targetWidth / metadata.width;
                    //         targetHeight = metadata.height * scale;
                    //         if(targetHeight > 1080) {
                    //             scale = 1080 / metadata.height;
                    //             targetHeight = 1080;
                    //             targetWidth = metadata.width * scale;
                    //         }
                    //     }
                    // }
                    // if(height)

                    try {
                        if (format == 'auto') {
                            const content_type = res.headers['content-type'];
                            if(content_type) {
                                const content_format = String(content_type.split('/', 2)[1]).replace('jpg', 'jpeg');
                                if(content_format && ['jpeg', 'png', 'webp', 'gif', 'tiff', 'avif'].indexOf(content_format) >= 0) {
                                    format = content_format;
                                } else {
                                    format = 'png';
                                }
                            } else {
                                format = 'png';
                            }
                        }

                        if (format) {
                            const options = {};
                            if(quality && format != 'gif') {
                                options.quality = quality;
                            }
                            transform = transform.toFormat(format, options)
                        }

                        if (width || height) {
                            transform = transform.resize(width, height)
                        }
                    } catch(error) {
                        console.error(error);
                        reject(`Encountered an error trying to resize: ${error.message}`);
                        return;
                    }

                    resolve([transform, format]);
                  });
              }
        });

        request.on('error', (err) => {
          console.error(`Encountered an error trying to make a request: ${err.message}`);
          reject('Error doing the request');
        });
    });
}

const server = express();

const port = 8080;

server.get('/custom-cgi/image/health', async(req, res) => {
    res.status(200).type('text/plain').end('SUCCESS');
});

server.get('*', async (req, res) => {

    const request_uri = req.path;

    //console.log(request_uri);

    if(!/^\/custom-cgi\/image\//.test(request_uri)) {
        res.status(404).type('text/plain').end('Cannot GET ' + request_uri);
        return;
    }

    const request_path = request_uri.replace(/^\/custom-cgi\/image\//, '');
    const request_path_parameters = request_path.split('/');
    const parameters = request_path_parameters.slice(0,1)[0].split(',');
    let path = request_path_parameters.slice(1).join('/');

    if(path == '' || /^\/*custom-cgi\/+image\//.test(path)) {
        res.status(404).type('text/plain').end('Cannot GET ' + request_uri);
        return;
    }

    console.error(path);

    let widthString;
    let heightString;
    let qualityString;
    let format;

    for(let param of parameters) {
        const keyvalue = param.split('=');
        const value = keyvalue.slice(1).join('=');
        switch(keyvalue.slice(0,1)[0]) {
            case 'w':
                widthString = value;
                break;
            case 'q':
                qualityString = value;
                break;
            case 'h':
                heightString = value;
                break;
            case 'f':
                format = value;
                break;
            case 'p':
                path = value;
                break;
        }
    }

    if(format == 'auto') {
        // jpeg, png, webp, gif, tiff, avif
        res.setHeader('Vary', 'Accept');
        const accept = (req.headers['accept'] || '').split(',');
        for(let accept_format of accept) {
            if(/^image\//.test(accept_format)) {
                const accept_format_image = accept_format.replace(/^image\//, '').replace('jpg', 'jpeg');
                if(['jpeg', 'png', 'webp', 'gif', 'tiff', 'avif'].indexOf(accept_format_image) >= 0) {
                    format = accept_format_image;
                    break;
                }
            }
        }
    }

    // Parse to integer if possible
    let width, height, quality
    if (widthString) {
        width = parseInt(widthString)
        if(width < 0) {
            res.status(400).send();
            return;
        }
        if(width > MAX_WIDTH) {
            width = MAX_WIDTH;
        }
    }
    if (heightString) {
        height = parseInt(heightString)
        if(height < 0) {
            res.status(400).send();
            return;
        }
        if(height > MAX_HEIGHT) {
            height = MAX_HEIGHT;
        }
    }
    if(width && height && (width*height)>(MAX_WIDTH*MAX_HEIGHT)) {
        res.status(400).send();
        return;
    }
    if (qualityString) {
        quality = parseInt(qualityString);
        if(quality < 0 || quality > 100) {
            res.status(400).send();
            return;
        }
    }

    const host = req.headers['host'] || '';

    if(host == '' || (ALLOWED_HOSTS.size > 0 && !ALLOWED_HOSTS.has(host.toLowerCase()))) {
        res.status(403).send();
        return;
    }

    const isHttps = req.secure || req.protocol == 'https' || req.socket?.encrypted || req.headers?.['x-forwarded-proto']?.split?.(/\s*,\s*/)?.[0] == 'https';

    let targetUrl;

    for(let [forceHost, baseUrl] of FORCE_HOSTS) {
        if(forceHost == '*' || host == forceHost) {
            let baseUrlWithProtocol = baseUrl.startsWith('http://') || baseUrl.startsWith('https://') ? baseUrl : `${isHttps ? 'https' : 'http'}://${baseUrl}`;
            baseUrlWithProtocol = baseUrlWithProtocol.replace(/\/+$/, '');
            targetUrl = `${baseUrlWithProtocol}/${path}`;
            break;
        }
    }

    if(!targetUrl) {
        targetUrl = `${isHttps ? 'https' : 'http'}://${host}/${path}`;
    }

    try {
        // TODO: check image info to avoid resizing a extremely large image

        // Get the resized image
        const stream = await resize(targetUrl, format, width, height, quality);
        format = stream[1];
        // Set the content-type of the response
        res.type(`image/${format || 'png'}`)
        // 1 week
        res.setHeader('Cache-Control', 'max-age=604800');
        res.setHeader("Expires", new Date(Date.now() + 604800000).toUTCString());
        stream[0].pipe(res);
    } catch (error) {
        console.error('image thum generation error is: ', String(error));
        res.status(500).send()
        return;
    }
});

server.listen(port, () => {
    console.log(`Server listening for HTTP requests on port ${port}`);
})
