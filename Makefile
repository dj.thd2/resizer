.PHONY: init build up down shell lint

init:
	make down
	rm -rf ./backend/node_modules
	make build
	docker-compose run --no-deps --rm backend npm install --unsafe-perm

build:
	docker-compose build

up:
	docker-compose up --no-build ; \

down:
	docker-compose down --remove-orphans

shell:
	docker-compose exec backend /bin/sh || docker-compose run --rm --no-deps backend "/bin/sh"

lint:
	make down
	docker-compose run --no-deps --rm backend ./node_modules/.bin/eslint --max-warnings=0 'index.js'

