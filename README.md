# resizer

## Description

Simple Node.js + Express service to resize images, implementing a subset of features of [Cloudflare Image Resizing](https://developers.cloudflare.com/images/image-resizing/) by using
[Sharp](https://sharp.pixelplumbing.com/) library to manipulate images, including automatic format conversion compatibility by parsing the sent `Accept` header by the client.

To be compatible with standard Cloudflare cache, we expose the service at path `/custom-cgi/image/` instead of `/cdn-cgi/image/`, so to move from Cloudflare Image Resizer to this
implementation, just replace `cdn-cgi` with `custom-cgi` when generating the image URL.

